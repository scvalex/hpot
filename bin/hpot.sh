#!/bin/bash

set -e -o pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
VAR="$HOME/var/hpot"
mkdir -p "$VAR"
"$DIR/hpot" "$@" > "$VAR/hpot.$(date +%Y-%m-%d).log"
