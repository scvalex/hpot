use slog::{error, info, Record, Serializer, KV};
use std::io::{BufRead, BufReader, Write};
use std::net::{TcpListener, TcpStream};
use std::time::Duration;

pub mod hpot_log;
use hpot_log::LOG;

#[derive(Debug)]
struct ClientInfo {
    method: Option<String>,
    path: Option<String>,
    version: Option<String>,
    authorization: Option<String>,
    other_lines: Vec<String>,
}

impl ClientInfo {
    fn new() -> ClientInfo {
        ClientInfo {
            method: None,
            path: None,
            version: None,
            authorization: None,
            other_lines: vec![],
        }
    }
}

impl KV for ClientInfo {
    fn serialize(&self, _: &Record, serializer: &mut dyn Serializer) -> slog::Result {
        if let Some(ref method) = self.method {
            serializer.emit_str("client_info.method", method)?;
        }
        if let Some(ref path) = self.path {
            serializer.emit_str("client_info.path", path)?;
        }
        if let Some(ref version) = self.version {
            serializer.emit_str("client_info.version", version)?;
        }
        Ok(())
    }
}

fn handle_client(mut stream: TcpStream) -> Result<ClientInfo, failure::Error> {
    stream.set_read_timeout(Some(Duration::from_secs(1)))?;
    stream.set_write_timeout(Some(Duration::from_secs(1)))?;
    let mut ci = ClientInfo::new();
    let mut first_line = true;
    for line in BufReader::new(&stream).lines() {
        let line = line?;
        if first_line {
            let mut line = line.split(' ');
            ci.method = line.next().map(|s| s.to_string());
            ci.path = line.next().map(|s| s.to_string());
            ci.version = line.next().map(|s| s.to_string());
            first_line = false;
        } else {
            if line.is_empty() {
                break;
            }
            if line.starts_with("Authorization:") {
                ci.authorization = Some(line.to_string());
            } else {
                ci.other_lines.push(line);
            }
        }
    }
    stream.write_all(
        b"HTTP/1.1 401 Unauthorized status\r\nWWW-Authenticate: Basic realm=\"Realm\"\r\n\r\n",
    )?;
    Ok(ci)
}

pub fn run_http_logger() -> Result<(), failure::Error> {
    info!(LOG, "Http Logger!");
    let listener = TcpListener::bind("0.0.0.0:8080")?;
    for stream in listener.incoming() {
        match stream {
            Err(err) => error!(LOG, "Error: {}", err),
            Ok(stream) => {
                let ip_addr = stream
                    .peer_addr()
                    .map(|addr| addr.ip().to_string())
                    .unwrap_or_else(|err| format!("error: {}", err));
                info!(LOG, "New client"; "ip_addr" => &ip_addr);
                match handle_client(stream) {
                    Ok(ci) => info!(LOG, "Client disconnected"; "ip_addr" => &ip_addr, ci),
                    Err(err) => {
                        info!(LOG, "Client error"; "ip_addr" => &ip_addr, "error"=>err.to_string())
                    }
                }
            }
        }
    }
    Ok(())
}
