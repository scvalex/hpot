use failure::bail;

use hpot::*;

fn main() -> Result<(), failure::Error> {
    let args: Vec<String> = std::env::args().collect();
    let args: Vec<&str> = args.iter().map(|str| str.as_ref()).collect();
    match args.as_slice() {
        [_, "http-logger"] => run_http_logger(),
        [_, "--version"] => {
            println!("{}", env!("CARGO_PKG_VERSION"));
            Ok(())
        }
        _ => bail!("not enough arguments"),
    }
}
