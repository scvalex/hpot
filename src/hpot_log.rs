use chrono::Utc;
use lazy_static::lazy_static;
use serde_json::{json, Map, Value};
use slog::{o, Drain, Key, OwnedKVList, Record, Serializer, KV};
use std::fmt::Arguments;

#[derive(Copy, Clone)]
pub struct HpotLogger;

lazy_static! {
    pub static ref LOG: slog::Logger = slog::Logger::root(HpotLogger.fuse(), o!());
}

struct KVMap(Map<String, Value>);

impl Serializer for KVMap {
    fn emit_arguments(&mut self, key: Key, fmt: &Arguments) -> Result<(), slog::Error> {
        let KVMap(map) = self;
        map.insert(key.to_string(), json!(fmt.to_string()));
        Ok(())
    }
}

impl Drain for HpotLogger {
    type Ok = ();
    type Err = failure::Error;
    fn log(&self, record: &Record, values: &OwnedKVList) -> Result<(), failure::Error> {
        let timestamp = Utc::now().to_rfc3339();
        let mut kvs = KVMap(Map::new());
        record.kv().serialize(record, &mut kvs)?;
        values.serialize(record, &mut kvs)?;
        let mut kvs = kvs.0;
        let metadata = json!({
            "timestamp": timestamp,
            "level": record.level().to_string(),
            "message": record.msg(),
        });
        for (k, v) in metadata.as_object().expect("not a map; wat?") {
            kvs.insert(k.to_string(), v.clone());
        }
        println!("{}", Value::Object(kvs).to_string());
        Ok(())
    }
}
